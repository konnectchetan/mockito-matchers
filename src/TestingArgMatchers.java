import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class TestingArgMatchers {
    SomeService s;
    Process p;
    @Before
    public void init(){
        s = Mockito.mock(SomeService.class);
        p =new Process(s);
    }
    @Test
    public void testInt(){
        Mockito.when(s.somethingOnInt(Matchers.anyInt())).thenReturn(10);
        String out = p.processInt(23);
        Assert.assertEquals("Output is "+10,out);
    }
    @Test
    public void testFloat(){
        Mockito.when(s.somethingOnFloat(Matchers.anyFloat())).thenReturn(33);
        String out = p.processFloat(23);
        Assert.assertEquals("Output is "+33,out);
    }
    @Test
    public void testBool(){
        Mockito.when(s.somethingOnBool(Matchers.anyBoolean())).thenReturn(10);
        String out = p.processBool(true);
        Assert.assertEquals("Output is "+10,out);
    }
    @Test
    public void testString(){
        Mockito.when(s.somethingOnString(Matchers.anyString())).thenReturn(10);
        String out = p.processString("Hello");
        Assert.assertEquals("Output is "+10,out);
    }
    @Test
    public void testList(){
        List l=null;
        Mockito.when(s.somethingOnList(Matchers.anyList())).thenReturn(0);
        String out = p.processList(l);
        Assert.assertEquals("Output is "+0,out);
    }
}
