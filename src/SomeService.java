import java.util.List;

public interface SomeService {
    public int doSomething();

    public int somethingOnInt(int a);
    public int somethingOnBool(boolean a);
    public int somethingOnString(String a);
    public int somethingOnList(List a);
    public int somethingOnFloat(Float a);

}
