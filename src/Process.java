import java.util.List;

public class Process {
    private SomeService service;
    public Process(SomeService s){
        this.service=s;
    }
    public String processing(){
        int number = service.doSomething();
        return "After doing something the number is 10";
    }
    public String processInt(int a){
        int number = service.somethingOnInt(a);
        return "Output is "+number;
    }
    public String processBool(boolean a){
        return "Output is "+service.somethingOnBool(a);
    }
    public String processFloat(float a){
        return "Output is "+service.somethingOnFloat(a);
    }
    public String processList(List a){
        return "Output is "+service.somethingOnList(a);
    }
    public String processString(String a){
        return "Output is "+service.somethingOnString(a);
    }
}
