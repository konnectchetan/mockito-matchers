import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class TestingService {
    @Test
    public void testingProcessMethod(){

        SomeService s = Mockito.mock(SomeService.class);
        Mockito.when(s.doSomething()).thenReturn(12,20,30);

        Process p =new Process(s);
        String msg = p.processing();
        System.out.println(msg);
        Assert.assertEquals("After doing something the number is 10",msg);

        //verify(s).doSomething();

        String msg2=p.processing();
        System.out.println(msg2);
        Assert.assertEquals("After doing something the number is 10",msg2);

        //verify(s,times(1)).doSomething();

        String msg3=p.processing();
        System.out.println(msg3);
        Assert.assertEquals("After doing something the number is 10",msg3);
        //verify(s,atLeast(3)).doSomething();
        verify(s,atMost(3)).doSomething();
    }
}
